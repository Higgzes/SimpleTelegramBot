cmake_minimum_required(VERSION 3.16)
project(bot_example)

set(CMAKE_CXX_STANDARD 23)
set(Boost_USE_MULTITHREADED ON)

find_package(cpr REQUIRED)
find_package(Threads REQUIRED)
find_package(OpenSSL REQUIRED)
find_package(Boost COMPONENTS system REQUIRED)
find_package(PkgConfig)
pkg_check_modules(LIBMYSQLCLIENT REQUIRED mysqlclient)

add_executable(bot_example src/main.cpp src/botcontrol.cpp src/dbconnect.cpp src/MessageRules.cpp src/tasker.cpp)

target_link_libraries(bot_example PRIVATE /usr/local/lib/libTgBot.a
  ${CMAKE_THREAD_LIBS_INIT} ${OPENSSL_LIBRARIES} ${Boost_LIBRARIES} ${CURL_LIBRARIES} ${LIBMYSQLCLIENT_LIBRARIES}
  cpr::cpr)
