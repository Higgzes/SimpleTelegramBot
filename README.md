# SimpleTelegramBot

Telegram bot example that can use Telegram API using [tgbot-cpp](https://github.com/reo7sp/tgbot-cpp) C++ library also has a sync with Database using  MySQL-dev libraries. For HTTP(S) methods bot use [Cpr](https://github.com/libcpr/cpr) wrapper around libcurl. Potentially, the functionality of the Telegram bot can be expanded.

## Installation

Linux Script

[Run script](https://codeberg.org/Higgzes/SimpleTelegramBot/src/branch/main/run) for clean automation instalation for Debian 12+/Ubuntu 23.04+ dist (for WSL make sure [Systemd](https://devblogs.microsoft.com/commandline/systemd-support-is-now-available-in-wsl/) enabled). 
GCC version 11+ required. 
The script will install all dependencies and default-MySQLServer. 
  IMPORTANT: Script will make a first mySQL-Server initialization, create a new DataBase for the bot and set the default password for user root.
```bash
export DB_ROOT_PASSWORD="Root12345!";
```
To run script make 'run' executable using command
```bash
sudo chmod +x run
```
and run the scrypt
```bash
sudo ./run
```
After installation process finish run the program
```bash
cd build && ./bot_example
```
IMPORTANT: If you already have initializated MySQLServer, plase use manual installation:

### Manual Installation:
You can install dependencies with these commands:
```bash
apt install cmake g++ git make binutils cmake libboost-system-dev libssl-dev zlib1g-dev libcurl4-openssl-dev default-mysql-server default-libmysqlclient-dev pkg-config -y
```
Create a new DataBase and User for Bot. Here is defaults parameters from 'main.cpp' and 'dbconnect.h' (using Default Port). It can be  change or customizatite for your DB server
```c++
std::string user_def = "bot";       //Username
std::string pwd_def = "bot12345";   //Password
std::string db_def = "bot_db";      //DataBase name
std::string server = "localhost";   //from dbconnect.h - Server address
std::string chats_table_name = "chats" //from dbconnect.h - Table name
```
To create the Table for Bot Database use MYSQL command:
```bash
create table chats (ChatID bigint PRIMARY KEY, TgChatType enum("private", "group"));
```
Install [Tgbot-cpp](https://github.com/reo7sp/tgbot-cpp) and [Cpr](https://github.com/libcpr/cpr).
Finally, Run CMake, Make and launch the program: bot_example

To compile without CMake in '/src' directory run
```bash
g++ main.cpp botcontrol.cpp dbconnect.cpp MessageRules.cpp tasker.cpp -std=c++23 -lTgBot -lmysqlclient -lpthread -lssl -lcrypto -lcpr
```

## Usage
Current version of the project is for test of Telegram Bot API functionality only.

Bot uses a database to store Telegram Chatid and ChatType (Private or Group) that is a member of. Bot handle all incoming messages (if he has access to read messages) and react only if the user reply to Bot message or if Bot was mentioned with '@' in the message. There are 3 commands, that bot can process. 

* If the user's message contains the word "updatedb", bot remove from DataBase table records of chats that bot is not member of anymore.
* If the user's message contains the word "bitcoin", the bot will create a task to get the current price of Bitcoin. To do this, we use a GET request to CoinBase API {https://api.coinbase.com/v2/prices/BTC-USD/spot} and extract the price. After the bot receives the data from CoinBase, it will send a message with the current Bitcoin price.
* It simply replies "Hello, I'm bot" to the user's message "Hi bot".

All parameters can be customized and extended for your usage.

