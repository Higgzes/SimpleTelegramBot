#include "tasker.h"

tasker::tasker (task_type type, long chatID, long messageID)
	: task_thr(std::thread(&tasker::do_task, this)),type(type)
{
	//using separate thread for the new task
	task_thr.detach();
	result = {false, chatID, messageID};
}

void tasker::check_btc_price() {

	std::string url = "https://api.coinbase.com/v2/prices/BTC-USD/spot";
	cpr::Response r = cpr::Get(cpr::Url{url}, cpr::Timeout{5000});

	if(r.error) {
		std::cout<<"API request to "<<url<<"::"<<r.error.message<<std::endl;
		result.message = "Sorry, I cannot help you at the moment";
		result.is_ready = true;
		return;
	}
	std::this_thread::sleep_for (std::chrono::seconds(10));
	result.message = "Bitcoin price is $";
	std::erase_if(r.text, [](char c){ return (!std::isdigit(c) && c != '.');});
	result.message += r.text;
	result.is_ready = true;
}

void tasker::do_task() {
	switch (type) {
		case task_type::CHECK_BTC_PRICE: check_btc_price(); break;
	}
}

