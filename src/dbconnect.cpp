#include "dbconnect.h"

db_connect::db_connect(std::string username,
						std::string password,
						std::string db_name,
						std::string server,
						std::string chats_table_name)
		:username(username),
		 password(password),
		 db_name(db_name),
		 server(server),
		 chats_table_name(chats_table_name)
{
	con = mysql_connection_setup();
    if (mysql_refresh(con, 0)) return;
    //using separate thread for reading data from Database
    read_from_db_thr = std::thread(&db_connect::read_from_db, this);
    read_from_db_thr.detach();
}

db_connect::~db_connect () {
	mysql_close(con);
}

MYSQL* db_connect::mysql_connection_setup(){
    MYSQL *connection = mysql_init(NULL);

    if(!mysql_real_connect(connection, server.c_str(), username.c_str(), password.c_str(), db_name.c_str(), 0, NULL, 0)){
        std::cout << "Error connection to db: " << mysql_error(connection) << std::endl;
    }

    return connection;
}
void db_connect::addchat(long chatid, std::string chattype) {

	//Add new chat to Map 'chat_table' and write data to Database
	chat_table.insert({chatid, chattype});
	std::unique_lock<std::mutex> lock(mu);
	if (mysql_refresh(con, 0))
		{std::cout<<"Chat was not added. Connection to DB error (ID#"<<chatid<<")"<<std::endl;
		return;}


	std::string query = "INSERT INTO ";
	query += chats_table_name;
	query += " VALUES(";
	query += std::to_string(chatid);
	query += ",'";
	query += chattype;
	query += "');";
	mysql_query(con, query.c_str());
	lock.unlock();
	
	std::cout<<"New chat added (#"<<chatid<<")"<<std::endl;
}

void db_connect::read_from_db(){

	//Map 'chat_table' will be used for reading data from Database (ChatID and ChatType)
    MYSQL_RES *res;
    MYSQL_ROW row;
    std::unique_lock<std::mutex> lock(mu);
    std::string query = "SELECT * FROM ";
    query += chats_table_name;
    query += ";";
    mysql_query(con, query.c_str());
    res = mysql_use_result(con);
    lock.unlock();
    while ((row = mysql_fetch_row(res)) != NULL) {
    	chat_table.insert({std::stol(row[0]), row[1]});
    }
    mysql_free_result(res);
	std::cout<<"Sync successfully (Database used: "
			<<db_name<<".chats, records: "<<chat_table.size()<<")"<<std::endl;
}

bool db_connect::find_chat(long id) {
	//C++20 simple return results if key (ChatID) exist in Map container
	return chat_table.contains(id);
}

void db_connect::remove_chat(long id) {
	//Delete new chat from Map 'chat_table' and delete data from Database
	if (!find_chat(id)) return;
	chat_table.erase(id);
	std::unique_lock<std::mutex> lock(mu);
	if (mysql_refresh(con, 0))
			{std::cout<<"Chat cannot be removed. Connection to DB error (ID#"<<id<<")"<<std::endl;
			return;}

		std::string query = "DELETE FROM ";
		query += chats_table_name;
		query += " WHERE chatID =";
		query += std::to_string(id);
		query += ";";
		mysql_query(con, query.c_str());
        	lock.unlock();
		std::cout<<"Chat removed (#"<<id<<")"<<std::endl;
}

std::vector<long> db_connect::get_chat_IDs () {
	auto kv = std::views::keys(chat_table);
	return {kv.begin(), kv.end()};
}
