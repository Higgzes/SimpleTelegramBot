#include "MessageRules.h"

// Rules for the incoming messages can be adjusted in this class

MessageRules::MessageRules(const TgBot::Message::Ptr& message, const std::string& botusername)
	: is_reply_needed(false),
	  is_task_needed(false),
	  is_update_db_needed(false)

{
//Bot will react if user reply to Bot message or if Bot was mentioned with '@'
	if ( (message->replyToMessage && (message->replyToMessage->from->username == botusername) )
		|| ( (message->entities.size() && message->entities[0]->type == TgBot::MessageEntity::Type::Mention
				&& message->text.find(botusername) != std::string::npos) ) )

			additional_check(message);
};


void MessageRules::additional_check(const TgBot::Message::Ptr& message) {
	std::string msg = message->text;
	std::transform(msg.begin(), msg.end(), msg.begin(), ::tolower);
	
	//using C++23 std::string.contains method
	
	if (msg.contains("hi bot")) {
		reply_message = "Hello, I'm bot";
		is_reply_needed = true;
		return;
	}

	if (msg.contains("updatedb")) {
		reply_message = "Ok";
		is_reply_needed = true;
		is_update_db_needed = true;
		return;
	}

	if (msg.contains("bitcoin")) {
		task = std::make_unique<tasker>
				(tasker::task_type::CHECK_BTC_PRICE, message->chat->id, message->messageId);
		is_task_needed = true;
		return;
	}

};
