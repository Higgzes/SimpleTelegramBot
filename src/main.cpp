// g++ main.cpp botcontrol.cpp dbconnect.cpp MessageRules.cpp tasker.cpp -std=c++23 -lTgBot -lmysqlclient -lpthread -lssl -lcrypto -lcpr
// DB Table parameters:
// create table chats (ChatID bigint PRIMARY KEY, TgChatType enum("private", "group"));
// Default DB: username: 'bot', password 'bot12345', db_name = 'bot_db'. It can be changed below.
// Default DB server: 'localhost', port: default. It can be changed in dbconnect class.
// Default DB tablename for sync chatsID = 'chats'. It can be changed in botcontrol class class dbconnect class.


#include "botcontrol.h"


int main() {
	std::string bot_token;
	std::string db_user, user_def = "bot";
	std::string db_user_password, pwd_def = "bot12345";
	std::string db_name, db_def = "bot_db";

	std::cout << "Enter Bot Token (example '1234567890:AA44fffBBB555CCCCcccFF667HH'): ";
	std::getline (std::cin, bot_token);

	if (bot_token == "")
		{std::cout<<"Invalid token name"<<std::endl; return 0;}

	std::cout << "[Optional] Enter DataBase Username: (Empty for default): ";
	std::getline (std::cin, db_user);
	if (db_user == "") {db_user = user_def; std::cout << "Default username: "<<db_user<<std::endl;}

	std::cout << "[Optional] Enter DataBase Password: (Empty for default): ";
	std::getline (std::cin, db_user_password);
	if (db_user_password == "") {db_user_password = pwd_def; std::cout << "Default password: "<<db_user_password<<std::endl;}

	std::cout << "[Optional] Enter DataBase Name: (Empty for default): ";
	std::getline (std::cin, db_name);
	if (db_name == "") {db_name = db_def; std::cout << "Default DataBase: "<<db_name<<std::endl;}

	bot_control connect_TgBot(bot_token, db_user, db_user_password, db_name);

return 0;
}

