#pragma once
#include "MessageRules.h"
#include "dbconnect.h"
#include "tasker.h"

class bot_control {

	TgBot::Bot bot;
    TgBot::EventBroadcaster ev_br;
    TgBot::EventHandler ev_handler;
    db_connect db_connection;
    std::vector<std::unique_ptr<tasker>> tasks;
    std::string botusername;

public:
	bot_control(const std::string&,
				const std::string&,
				const std::string&,
				const std::string&
				);

private:
	void poll_old_updates();
	void start ();
	void onMessage_rules(TgBot::Message::Ptr);
	void check_chat_in_db(long, int);
	void actions();
	void check_all_chats();
	bool is_member_of_chat(long);
};

