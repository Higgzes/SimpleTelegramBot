#pragma once
#include "common.h"

class db_connect {

	std::string username;
	std::string password;
	std::string db_name;
	std::string server;
	std::string chats_table_name;
	std::thread read_from_db_thr;
	MYSQL *con;
	std::unordered_map<long, std::string> chat_table;

public:
	db_connect(std::string,
				std::string,
				std::string,
				std::string server = "localhost",
				std::string chats_table_name = "chats"
				);
	db_connect() = delete;
	~db_connect();
	void addchat(long, std::string);
	bool find_chat(long);
	void remove_chat(long);
	std::vector<long> get_chat_IDs();

private:
	MYSQL* mysql_connection_setup();
	void read_from_db();
	std::mutex mu;
};
