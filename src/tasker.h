#pragma once
#include "common.h"


class tasker {

	std::thread task_thr;

public:

struct result {
		bool is_ready;
		long orig_chatID;
		long orig_messageID;
		std::string message;
		operator bool() {return is_ready ? true : false;}
	} result;

	enum task_type {
		CHECK_BTC_PRICE,
		}
		type;

	tasker (task_type, long, long);
	tasker () = delete;

private:
	void do_task();
	void check_btc_price();
};
