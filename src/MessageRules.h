#pragma once
#include "common.h"
#include "tasker.h"

class MessageRules {
	bool is_reply_needed, is_task_needed, is_update_db_needed;
	std::string reply_message;
	std::unique_ptr<tasker> task;

public:

	MessageRules(const TgBot::Message::Ptr&, const std::string&);

	bool need_instant_reply() {return is_reply_needed;}
	bool need_create_task() {return is_task_needed;}
	bool need_update_chats_db() {return is_update_db_needed;}
	std::string getMessage () {return reply_message;}
	std::unique_ptr<tasker> getTask () {return std::move(task);}

private:
	void additional_check(const TgBot::Message::Ptr&);
};
