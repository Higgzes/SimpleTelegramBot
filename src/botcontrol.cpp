#include "botcontrol.h"

bot_control::bot_control
	(const std::string& token,
	 const std::string& db_user,
	 const std::string& db_password,
	 const std::string& db_name
	 )
// Table name for chatsID can be added additionally in db_connection() initialization (if table_name is not default)
	: bot(token), ev_handler (ev_br), db_connection(db_user, db_password, db_name)
{

	try {
	poll_old_updates();
	} catch (...) {printf("ERROR: Unable to get updates. Terminated"); return;}

	botusername = bot.getApi().getMe()->username;

	start();
}

void bot_control::poll_old_updates()
{
	//old events are not processing by the bot

	TgBot::TgLongPoll old_long_poll(bot);
	auto nu = bot.getApi().getUpdates();
	std::cout<<"Receiving updates ("<<nu.size()<<" event(s) processing)"<<std::endl;
	while (nu.size()) {
		old_long_poll.start();
		nu = bot.getApi().getUpdates();
	}
}

void bot_control::start() {

	//Bot will react on AnyMessage and EditedMessage only

	ev_br = bot.getEvents();
	auto handler = [this](TgBot::Message::Ptr message){onMessage_rules(std::move(message));};
	ev_br.onAnyMessage(handler);
    ev_br.onEditedMessage(handler);

	TgBot::TgLongPoll new_long_poll(&(bot.getApi()), &ev_handler,0,0, nullptr);

	std::cout<<"Bot is live. Handling incoming messages..."<<std::endl;

	while (1) {

		//Catching all Errors during the loop and trying to reconnect each time

		// new_long_poll.start() - processing new events
		try {new_long_poll.start();}
			catch (...) {std::cout<<"Error pulling updates. Trying to re-connect... \n";}

		// actions() - check which scheduled tasks are ready
		try {actions();}
			catch (...) {std::cout<<"Unknown Error. Trying to re-connect... \n";}
	}
}

void bot_control::onMessage_rules (TgBot::Message::Ptr message) {

	// Ignore message that the bot leave the chat
	if (message->leftChatMember	&& message->leftChatMember->username == botusername)
				return;
	//check is incoming message from the chat, that already exist in database
	if (!message->editDate)
		check_chat_in_db(message->chat->id, static_cast<int>(message->chat->type));

	MessageRules rules(message, botusername);

	if (rules.need_instant_reply()) {
		bot.getApi().sendMessage
				(message->chat->id, rules.getMessage(), 0, message->messageId);

		//additional rule to update chats database
		if (rules.need_update_chats_db()) check_all_chats();
		return;
	}

	if (rules.need_create_task()) tasks.push_back(rules.getTask());
}

void bot_control::check_chat_in_db(long chat_id, int chat_type){
	if (db_connection.find_chat(chat_id)) return;
	switch (chat_type) {
					case 0: db_connection.addchat(chat_id, "private"); break;
					case 1: db_connection.addchat(chat_id, "group"); break;
	}
}

void bot_control::actions() {

	// check all tasks and process action (send message) if the results of the task is ready
	// and delete the task after that
	std::erase_if(tasks,
			[this](std::unique_ptr<tasker>& task){
				if (task->result) {
					bot.getApi().sendMessage
						(task->result.orig_chatID, task->result.message, 0, task->result.orig_messageID);
					return true;
				}
				return false;}
			);
}

void bot_control::check_all_chats() {

	auto chatIDs = db_connection.get_chat_IDs();
	for (auto && i : chatIDs) is_member_of_chat(i);
}

bool bot_control::is_member_of_chat(long chatid) {

	//Catch error if Bot unable to send action to the chat.
	//In this case chat will be deleted from the database. Bot is not member of this chat
	try {
	bot.getApi().sendChatAction(chatid, "typing");
	} catch (TgBot::TgException& e) {db_connection.remove_chat(chatid); return false;}
	return true;
};
